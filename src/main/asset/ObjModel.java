package main.asset;

//@author David

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ObjModel {
    
    private static float[] verticesStatic;
    private static float[] texturesStatic;
    private static float[] normalsStatic;
    
    private static int[] vertexIndices;
    private static int[] textureIndices;
    private static int[] normalIndices;
    
    private static int currentVertex = 0;
    private static int currentTexture = 0;
    private static int currentNormal = 0;
    private static int currentIndex = 0;
    
    private static boolean hasTextureCoordinates = true;
    
    public float[] vertices;
    public float[] textureCoordinates;
    public float[] normals;
    public int[] indices;
    public boolean hasTextures = true;
    
    public static ObjModel load(String location){
        String file = loadFile(location);
        
        int verticesCnt = StringDecoder.occurences(file, "\nv ");
        int normalsCnt = StringDecoder.occurences(file, "\nvn ");
        int texturesCnt = StringDecoder.occurences(file, "\nvt ");
        int facesCnt = StringDecoder.occurences(file, "\nf ");

        verticesStatic = new float[verticesCnt*4];
        texturesStatic = new float[texturesCnt*4];
        normalsStatic = new float[normalsCnt*4];
        
        vertexIndices = new int[facesCnt*3];
        textureIndices = new int[facesCnt*3];
        normalIndices = new int[facesCnt*3];
        
        currentVertex = 0;
        currentTexture = 0;
        currentNormal = 0;
        currentIndex = 0;
        
        Scanner scan = new Scanner(file);
        
        while(scan.hasNextLine()){
            String line = scan.nextLine();
            line = StringDecoder.removeWhiteSpace(line);
            if(line.charAt(0) == '#'){
                //Handle Comment
            }else if(line.charAt(0) == 'v'){
                if(line.charAt(1) != 't' && line.charAt(1) != 'n' && line.charAt(1) != 'p'){
                    parseVertex(StringDecoder.removeWhiteSpace(line.substring(1)));
                }else if(line.charAt(1)=='t'){
                    parseTexture(StringDecoder.removeWhiteSpace(line.substring(2)));
                }else if(line.charAt(1)=='n'){
                    parseNormal(StringDecoder.removeWhiteSpace(line.substring(2)));
                }
            }else if(line.charAt(0) == 'f'){
                parseFace(StringDecoder.removeWhiteSpace(line.substring(1)));
            }
            else if(line.charAt(0) == 'g'){
                //Handle Group Tag
            }else if(line.charAt(0) == 'o'){
                //Handle Object Tag
            }else if(line.charAt(0) == 'u'){
                //Handle Library Usage
            }else if(line.charAt(0) == 'm'){
                //Handle Library Import
            }
        }
        
        return combineIndices();
    }
    
    private static void parseVertex(String value){
        int firstSpace = 0;
        while(value.charAt(firstSpace) != ' '){
            firstSpace++;
        }
        int secondSpace = firstSpace+1;
        while(value.charAt(secondSpace) != ' '){
            secondSpace++;
        }
        verticesStatic[currentVertex] = Float.parseFloat(value.substring(0, firstSpace));
        verticesStatic[currentVertex+1] = Float.parseFloat(value.substring(firstSpace+1, secondSpace));
        verticesStatic[currentVertex+2] = Float.parseFloat(value.substring(secondSpace+1));
        verticesStatic[currentVertex+3] = 1;
        currentVertex+=4;
    }
    
    private static void parseTexture(String value){
        int firstSpace = 0;
        while(value.charAt(firstSpace) != ' '){
            firstSpace++;
        }
        
        int secondSpace = value.indexOf(' ', firstSpace+1);
        
        if(secondSpace == -1){
            texturesStatic[currentTexture] = Float.parseFloat(value.substring(0, firstSpace));
            texturesStatic[currentTexture+1] = Float.parseFloat(value.substring(firstSpace+1));
            texturesStatic[currentTexture+2] = 0;
            texturesStatic[currentTexture+3] = 1;
        }else{
            texturesStatic[currentTexture] = Float.parseFloat(value.substring(0, firstSpace));
            texturesStatic[currentTexture+1] = Float.parseFloat(value.substring(firstSpace+1, secondSpace));
            texturesStatic[currentTexture+2] = Float.parseFloat(value.substring(secondSpace+1));
            texturesStatic[currentTexture+3] = 1;
        }
        
        currentTexture+=4;
    }
    
    private static void parseNormal(String value){
        int firstSpace = 0;
        while(value.charAt(firstSpace) != ' '){
            firstSpace++;
        }
        int secondSpace = firstSpace+1;
        while(value.charAt(secondSpace) != ' '){
            secondSpace++;
        }
        normalsStatic[currentNormal] = Float.parseFloat(value.substring(0, firstSpace));
        normalsStatic[currentNormal+1] = Float.parseFloat(value.substring(firstSpace+1, secondSpace));
        normalsStatic[currentNormal+2] = Float.parseFloat(value.substring(secondSpace+1));
        normalsStatic[currentNormal+3] = 1;
        currentNormal+=4;
    }
    
    private static void parseFace(String value){
        int firstSpace = value.indexOf(' ');
        int secondSpace = value.indexOf(' ', firstSpace+1);
        
        parseFaceGroup(value.substring(0, firstSpace));
        parseFaceGroup(value.substring(firstSpace+1, secondSpace));
        parseFaceGroup(value.substring(secondSpace+1));
    }
    
    private static void parseFaceGroup(String value){
        int vertexIndex = -1;
        int textureIndex = -1;
        int normalIndex = -1;
        
        int firstSlash = value.indexOf('/');
        int secondSlash = value.indexOf("/", firstSlash+1);
        boolean vert = false;
        boolean vertTex = false;
        boolean vertNorm = false;
        if(firstSlash == -1){
            vert = true;
        }else if(secondSlash == -1){
            vertTex = true;
        } else if(secondSlash == firstSlash+1){
            vertNorm = true;
        }
        
        if(vert){
            hasTextureCoordinates = false;
            vertexIndex = Integer.parseInt(value)-1;
        }else if(vertTex){
            vertexIndex = Integer.parseInt(value.substring(0, firstSlash))-1;
            textureIndex = Integer.parseInt(value.substring(firstSlash+1))-1;
        }else if(vertNorm){
            hasTextureCoordinates = false;
            vertexIndex = Integer.parseInt(value.substring(0, firstSlash))-1;
            normalIndex = Integer.parseInt(value.substring(secondSlash+1))-1;
        }else{
            vertexIndex = Integer.parseInt(value.substring(0, firstSlash))-1;
            textureIndex = Integer.parseInt(value.substring(firstSlash+1, secondSlash))-1;
            normalIndex = Integer.parseInt(value.substring(secondSlash+1))-1;
        }
        
        vertexIndices[currentIndex] = vertexIndex;
        textureIndices[currentIndex] = textureIndex;
        normalIndices[currentIndex] = normalIndex;
        
        currentIndex += 1;
    }
    
    private static ObjModel combineIndices(){
        if(hasTextureCoordinates){
            return combineIndicesWithTextures();
        }else{
            return combineIndicesWithoutTextures();
        }
    }
    
    private static ObjModel combineIndicesWithTextures(){
        Map<Integer, Integer> map = new HashMap();
        
        float[] newVertices = new float[verticesStatic.length];
        System.arraycopy(verticesStatic, 0, newVertices, 0, verticesStatic.length);

        float[] newTextures = new float[verticesStatic.length];
        for(int i=0;i<newTextures.length;i+=4){
            newTextures[i] = -1;
        }

        float[] newNormals = new float[verticesStatic.length];
        for(int i=0;i<newNormals.length;i+=4){
            newNormals[i] = -1;
        }

        int[] newIndices = new int[vertexIndices.length];
        System.arraycopy(vertexIndices, 0, newIndices, 0, vertexIndices.length);

        for(int i=0;i<vertexIndices.length;i++){
            int oldVertexIndex = vertexIndices[i];
            int oldTextureIndex = textureIndices[i];
            int oldNormalIndex = normalIndices[i];

            if(newNormals[oldVertexIndex*4] == -1 && newTextures[oldVertexIndex*4] == -1){
                copyVector4(newNormals, oldVertexIndex*4, normalsStatic, oldNormalIndex*4);
                copyVector4(newTextures, oldVertexIndex*4, texturesStatic, oldTextureIndex*4);
            }else if(vectorsEqual(newNormals, oldVertexIndex*4, normalsStatic, oldNormalIndex*4, 4) &&
                     vectorsEqual(newTextures, oldVertexIndex*4, texturesStatic, oldTextureIndex*4, 4)){
            }else{
                boolean skipStep = false;
                if(map.containsKey(oldVertexIndex)){
                    int newLocation = map.get(oldVertexIndex);
                    if(vectorsEqual(newNormals, newLocation*4, normalsStatic, oldNormalIndex*4, 4) &&
                       vectorsEqual(newTextures, newLocation*4, texturesStatic, oldTextureIndex*4, 4)){
                        newIndices[i] = newLocation;
                        skipStep = true;
                    }
                }
                if(!skipStep){
                    newVertices = increaseArraySize(newVertices,4);
                    newNormals = increaseArraySize(newNormals,4);
                    newTextures = increaseArraySize(newTextures,4);

                    newIndices[i] = (newVertices.length/4)-1;
                    int extendedIndex = newIndices[i];

                    copyVector4(newVertices, extendedIndex*4, verticesStatic, oldVertexIndex*4);
                    copyVector4(newNormals, extendedIndex*4, normalsStatic, oldNormalIndex*4);
                    copyVector4(newTextures, extendedIndex*4, texturesStatic, oldTextureIndex*4);

                    map.put(oldVertexIndex, extendedIndex);
                }
            }
        }

        return new ObjModel(newIndices,newVertices,newTextures,newNormals);
        
    }
    
    private static ObjModel combineIndicesWithoutTextures(){
        Map<Integer, Integer> map = new HashMap();
        
        float[] newVertices = new float[verticesStatic.length];
        System.arraycopy(verticesStatic, 0, newVertices, 0, verticesStatic.length);

        float[] newNormals = new float[verticesStatic.length];
        for(int i=0;i<newNormals.length;i+=4){
            newNormals[i] = -1;
        }

        int[] newIndices = new int[vertexIndices.length];
        System.arraycopy(vertexIndices, 0, newIndices, 0, vertexIndices.length);

        for(int i=0;i<vertexIndices.length;i++){
            int oldVertexIndex = vertexIndices[i];
            int oldNormalIndex = normalIndices[i];

            if(newNormals[oldVertexIndex*4] == -1){
                copyVector4(newNormals, oldVertexIndex*4, normalsStatic, oldNormalIndex*4);
            }else if(vectorsEqual(newNormals, oldVertexIndex*4, normalsStatic, oldNormalIndex*4, 4)){
            }else{
                boolean skipStep = false;
                if(map.containsKey(oldVertexIndex)){
                    int newLocation = map.get(oldVertexIndex);
                    if(vectorsEqual(newNormals, newLocation*4, normalsStatic, oldNormalIndex*4, 4)){
                        newIndices[i] = newLocation;
                        skipStep = true;
                    }
                }
                if(!skipStep){
                    newVertices = increaseArraySize(newVertices,4);
                    newNormals = increaseArraySize(newNormals,4);

                    newIndices[i] = (newVertices.length/4)-1;
                    int extendedIndex = newIndices[i];

                    copyVector4(newVertices, extendedIndex*4, verticesStatic, oldVertexIndex*4);
                    copyVector4(newNormals, extendedIndex*4, normalsStatic, oldNormalIndex*4);

                    map.put(oldVertexIndex, extendedIndex);
                }
            }
        }

        return new ObjModel(newIndices,newVertices,null,newNormals);
        
    }
    
    private static String loadFile(String url){
        StringBuilder builder = new StringBuilder();
        BufferedReader file = null;
        try {
            file = new BufferedReader(new FileReader(url));
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
            return null;
        }
        try {
            String temp;
            while((temp=file.readLine())!=null){
                if(temp.length()>0){
                    builder.append(temp).append("\n");
                }
            }
        } catch (IOException ex) {
            System.out.println(ex);
            return null;
        }
        return builder.toString();
    }
    
    private static void copyVector4(float[] dest, int destPos, float[] src, int srcPos){
        for(int i=0;i<4;i++){
            dest[destPos+i] = src[srcPos+i];
        }
    }
    
    private static float[] getVector4(float[] src, int start){
        float[] vec = new float[4];
        for(int i=0;i<4;i++){
            vec[i] = src[start+i];
        }
        return vec;
    }
    
    private static float[] setArraySize(float[] array, int size){
        float[] temp = new float[size];
        System.arraycopy(array, 0, temp, 0, array.length);
        return temp;
    }
    
    private static float[] increaseArraySize(float[] array, int amount){
        float[] temp = new float[array.length+amount];
        System.arraycopy(array, 0, temp, 0, array.length);
        return temp;
    }
    
    private static int[] setArraySize(int[] array, int size){
        int[] temp = new int[size];
        System.arraycopy(array, 0, temp, 0, array.length);
        return temp;
    }
    
    private static int[] increaseArraySize(int[] array, int amount){
        int[] temp = new int[array.length+amount];
        System.arraycopy(array, 0, temp, 0, array.length);
        return temp;
    }
    
    public static void printVectors(float[] vectors){
        if(vectors.length%4==0)
            for(int i=0;i<vectors.length;i+=4)
                System.out.println(vectors[i] + ", " + vectors[i+1] + ", " + vectors[i+2] + ", " + vectors[i+3]);
    }
    
    private static boolean vectorsEqual(float[] vec1, float[] vec2){
        for(int i=0;i<vec1.length;i++)
            if(vec1[i] != vec2[i])
                return false;
        return true;
    }
    
    private static boolean vectorsEqual(float[] vec1, int offset1, float[] vec2, int offset2, int size){
        for(int i=0;i<size;i++)
            if(vec1[i+offset1] != vec2[i+offset2])
                return false;
        return true;
    }
    
    private ObjModel(int[] indices, float[] vertices, float[] textures, float[] normals){
        this.indices = indices;
        this.vertices = vertices;
        this.textureCoordinates = textures;
        this.normals = normals;
        if(textures == null){
            hasTextures = false;
        }
    }
    
    public ModelChunk createChunk(String id){
        return new ModelChunk(id, hasTextures, vertices, textureCoordinates, normals, indices);
    }
    
}
