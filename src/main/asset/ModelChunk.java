package main.asset;

//@author David

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ModelChunk extends Chunk {
    
    private float[] vertices;
    private float[] textures;
    private float[] normals;
    private int[] indices;
    private boolean hasTextures;

    public ModelChunk(String id, boolean hasTextures, float[] vertices, float[] textureCoordinates, float[] normals, int[] indices) {
        super(id, Chunk.MODEL_CHUNK);
        this.vertices = vertices;
        this.textures = textureCoordinates;
        this.normals = normals;
        this.indices = indices;
        this.hasTextures = hasTextures;
    }
    
    public static ModelChunk load(ByteBuffer b, String id){
        boolean hasTextures;
        float[] vertices = null;
        float[] textures = null;
        float[] normals = null;
        int[] indices = null;
        
        if(b.get() == 0x01){
            hasTextures = true;
        }else{
            hasTextures = false;
        }
        
        while(b.hasRemaining()){
            byte type = b.get();
            if(type == 0x00){ // Vertex List
                int length = b.getInt();
                byte[] data = new byte[length];
                b.get(data);
                vertices = Utils.byteArrayToFloatArray(data);
            }else if(type == 0x01){ // Texture List
                int length = b.getInt();
                byte[] data = new byte[length];
                b.get(data);
                textures = Utils.byteArrayToFloatArray(data);
            }else if(type == 0x02){ // Normal List
                int length = b.getInt();
                byte[] data = new byte[length];
                b.get(data);
                normals = Utils.byteArrayToFloatArray(data);
            }else if(type == 0x03){ //Index List
                int length = b.getInt();
                byte[] data = new byte[length];
                b.get(data);
                indices = Utils.byteArrayToIntArray(data);
            }
        }
        
        return new ModelChunk(id, hasTextures, vertices, textures, normals, indices);
    }
    
    @Override
    public void write(StreamOut s){
        try {
            s.write(0x10);
            s.write(0x12);
            s.writeInt(this.id.length()*2);
            s.writeString(id);
            
            if(hasTextures){
                s.writeInt(1+ 1+4+(vertices.length*4)+ 1+4+(textures.length*4)+ 1+4+(normals.length*4)+ 1+4+(indices.length*4));
                s.write(0x01);
            }else{
                s.writeInt(1+ 1+4+(vertices.length*4)+ 1+4+(normals.length*4)+ 1+4+(indices.length*4));
                s.write(0x00);
            }
            
            s.write(0x00);
            s.writeInt(vertices.length*4);
            s.write(Utils.floatArrayToByteArray(vertices));
            
            if(hasTextures){
                s.write(0x01);
                s.writeInt(textures.length*4);
                s.write(Utils.floatArrayToByteArray(textures));
            }
            
            s.write(0x02);
            s.writeInt(normals.length*4);
            s.write(Utils.floatArrayToByteArray(normals));
            
            s.write(0x03);
            s.writeInt(indices.length*4);
            s.write(Utils.intArrayToByteArray(indices));
//            byte[] tester = Utils.intArrayToByteArray(indices);
//            int[] result = Utils.byteArrayToIntArray(tester);
//            System.out.println(Arrays.equals(indices, result));
//            System.out.println(indices.length + ", " + result.length);
//            System.out.println();
//            for(int i=0;i<indices.length;i++){
//                System.out.println(indices[i] + ", " + result[i]);
//            }
            
            
        } catch (IOException ex) {
            Logger.getLogger(ShaderChunk.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public float[] getVertices() {
        return vertices;
    }

    public void setVertices(float[] vertices) {
        this.vertices = vertices;
    }

    public float[] getTextureCoordinates() {
        return textures;
    }

    public void setTextureCoordinates(float[] textureCoordinates) {
        this.textures = textureCoordinates;
    }

    public float[] getNormals() {
        return normals;
    }

    public void setNormals(float[] normals) {
        this.normals = normals;
    }

    public int[] getIndices() {
        return indices;
    }

    public void setIndices(int[] indices) {
        this.indices = indices;
    }

    public boolean isHasTextures() {
        return hasTextures;
    }

    public void setHasTextures(boolean hasTextures) {
        this.hasTextures = hasTextures;
    }
    
    @Override
    public String toString(){
        ObjModel.printVectors(this.vertices);
        return "";
    }
    
}
